﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class Stay
    {
        private List<HotelRoom> roomList;
        private DateTime checkInDate;
        private DateTime checkOutDate;

        public DateTime CheckOutDate
        {
            get { return checkOutDate; }
            set { checkOutDate = value; }
        }

        public DateTime CheckInDate
        {
            get { return checkInDate; }
            set { checkInDate = value; }
        }

        public List<HotelRoom> RoomList
        {
            get { return roomList; }
            set { roomList = value; }
        }

        public Stay()
        {

        }

        public Stay(DateTime checkInDate, DateTime checkOutDate)
        {
            CheckInDate = checkInDate;
            CheckOutDate = checkOutDate;
        }
        
        public void AddRoom(HotelRoom hR)
        {
            RoomList.Add(hR);
        }
       
        public double CalculateTotal()
        {
            return (checkOutDate - CheckInDate).TotalDays;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
