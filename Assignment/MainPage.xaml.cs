﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assignment
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        List<HotelRoom> roomList = new List<HotelRoom>();
        List<HotelRoom> sRoomList = new List<HotelRoom>();
        List<Guest> guestList = new List<Guest>();
        List<object> Linklist = new List<object>();
        List<string> roomDetailsList = new List<string>();
        List<string> addRoomList = new List<string>();
        List<HotelRoom> bookedrooms = new List<HotelRoom>();
        private static List<Guest> Guests = new List<Guest>();

        Membership am = new Membership("Gold", 280);
        Membership bo = new Membership("Ordinary", 0);
        Membership co = new Membership("Silver", 190);
        Membership ed = new Membership("Gold", 10);

        public MainPage()
        {
            this.InitializeComponent();
            InitData();
        }

        public void InitData()
        {
            roomList.Add(new StandardRoom("101", "Single", 90));
            roomList.Add(new StandardRoom("102", "Single", 90));
            roomList.Add(new StandardRoom("201", "Twin", 110));
            roomList.Add(new StandardRoom("202", "Twin", 110));
            roomList.Add(new StandardRoom("203", "Twin", 110));
            roomList.Add(new StandardRoom("301", "Triple", 120));
            roomList.Add(new StandardRoom("302", "Triple", 120));
            roomList.Add(new DeluxeRoom("204", "Twin", 140));
            roomList.Add(new DeluxeRoom("205", "Twin", 140));
            roomList.Add(new DeluxeRoom("303", "Triple", 210));
            roomList.Add(new DeluxeRoom("304", "Triple", 210));

            Stay a = new Stay(Convert.ToDateTime("29/01/2019"), Convert.ToDateTime("02/02/2019"));
            Stay b = new Stay(Convert.ToDateTime("25/01/2019"), Convert.ToDateTime("31/01/2019"));
            Stay c = new Stay(Convert.ToDateTime("1/02/2019"), Convert.ToDateTime("6/02/2019"));
            Stay d = new Stay(Convert.ToDateTime("28/01/2019"), Convert.ToDateTime("10/02/2019"));

            guestList.Add(new Guest("Amelia", "S1234567A", a, am, true));
            guestList.Add(new Guest("Bob", "G1234567A", b, bo, true));
            guestList.Add(new Guest("Cody", "G2345678A", c, co, true));
            guestList.Add(new Guest("Edda", "S3456789A", d, ed, true));
        }


        public int numOfRooms(int rindex)
        {
            if (rindex == 204 || rindex == 202 || rindex == 201)
            {
                return 3;
            }

            else
            {
                return 2;
            }
        }

        //2.1.1
        /*register the paying guest’s Name and passport Number & The check-in and check-out date
         take the 4 strings and make a guest? */
        //1) take name/pp, look for user when "search" is pressed
        //2) if user exists, load their member status/points
        //if false, set their membership to false/0
        //3) add a new "stay" list(?)

        public void Particulars()
        {
            try
            {
                bool v = false;
                string GuestName = nogInput.Text;
                string PassportNum = pnInput.Text;
                //2.1.6	If an existing guest Checks-out and Checks-In for a subsequent date, the guest should not have to be re-created, but have the stay information updated.
                foreach (Guest a in guestList.ToList())
                {
                    if (a.Name != GuestName || a.PpNumber != PassportNum)
                    {
                        v = true;
                    }
                }

                if (v == true)
                {
                    DateTime DateIn = CheckinDate.Date.Value.DateTime;
                    DateTime DateOut = CheckoutDate.Date.Value.DateTime;
                    Stay x = new Stay(Convert.ToDateTime(DateIn), Convert.ToDateTime(DateOut));
                    guestList.Add(new Guest(GuestName, PassportNum, x, bo, true));
                    DisplayRooms();
                }
            }
            catch
            {
                error.Text = "Please input into all the fields";
            }
        }

        //2.1.2 The list view displays the information of the unoccupied rooms.

        public void DisplayRooms()
        {
            roomList = roomList.OrderBy(o => o.RoomNumber).ToList();
            roomDetailsList.Clear();
            if (roomDetailsList.Count == 0)
            {
                foreach (var r in roomList)
                {
                roomDetailsList.Add(r.RoomNumber.ToString() + "   " + r.RoomType.ToString() + "   $" + r.DailyRate.ToString());
                }
            }
            
            arInput.ItemsSource = null;
            arInput.ItemsSource = roomDetailsList;
        }

        //2.1.3 one room is selected at a time. Additional request can be made using the checkboxes, before clicking on Add Room. 
        //This will also incur additional cost to the guest(refer to Table 1).
        //Stay.cs identifier required? 

        public void AddRoom()
        {
            int addon = 0;
            int i = arInput.SelectedIndex;
            sRoomList.Add(roomList[i]);

            addRoomList.Clear();
            foreach(var r in sRoomList)
            {
                addRoomList.Add(r.RoomNumber.ToString() + "   " + r.RoomType.ToString() + "   $" + r.DailyRate.ToString());
            }
            foreach (HotelRoom b in sRoomList)
            {
                roomList.Remove(b);
            }
            rsInput.ItemsSource = null;
            rsInput.ItemsSource = addRoomList;
            DisplayRooms();
            if (addBreakfast.IsChecked == true)
            {
                addon += 20 * sRoomList.Count;
            }
            if (addWifi.IsChecked == true)
            {
                addon += 10; 
            }
            if (addBed.IsChecked == true)
            {
                addon += 25;
            }
        }
        
        //2.1.4 remove room button is used to remove a selected room. The room is released back into the pool of available rooms

        public void RemoveRoom()
        {
            string rstring = rsInput.SelectedItem.ToString();
            int i = rsInput.SelectedIndex;
            roomList.Add(sRoomList[i]);
            sRoomList.Remove(sRoomList[i]);
            addRoomList.Clear();
            foreach (var r in sRoomList)
            {
                addRoomList.Add(r.RoomNumber.ToString() + "   " + r.RoomType.ToString() + "   $" + r.DailyRate.ToString());
            }
            rsInput.ItemsSource = null;
            rsInput.ItemsSource = addRoomList;
            DisplayRooms();
        }

        //2.1.5 Guest can have more than one room selected. Clicking Check-In will assign the selected rooms to the guest, and it will not be available for other guest use.

        public void checkInBtn_Click(object sender, RoutedEventArgs e)
        {
            //displays in 2nd list
            Particulars();
            bookedrooms.Clear();
            bookedrooms.AddRange(sRoomList);

            foreach (Guest s in guestList)
            {
                if(s.Name == nogInput.Text || s.PpNumber == pnInput.Text)
                {
                    foreach (HotelRoom a in sRoomList)
                    {
                        s.AddRoom(a);
                    }
                }
            }

            DisplayRooms();

            sRoomList.Clear();
        }
        
        private void AddRoom_Click(object sender, RoutedEventArgs e)
        {
            AddRoom();
        }

        private void RemoveRoom_Click(object sender, RoutedEventArgs e)
        {
            RemoveRoom();
        }

        /*private void checkInBtn_Click(object sender, RoutedEventArgs e)
        {
            //3.1.1	Guest membership status.A paying guest is auto-included into the membership program the moment they check-in. A guest begins as an ordinary member and is only converted to a silver or gold membership once 100 or 200 points are earned, respectively.Guests will not lose their status once it is attained.
            Particulars();
            BRoom();
        }*/

        private void CheckRoomsAvailable_Click(object sender, RoutedEventArgs e)
        {
            DisplayRooms();
        }

        //2.2.1 Using the same text boxes used in guest registration, guest information can be retrieved either by Name or Passport number. Search button will utilize the existing text block and list view below it to display hotel rooms currently booked by the guest.


        public void Search()
        {
            foreach (Guest a in guestList)
            {
                string GuestName = nogInput.Text;
                string PassportNum = pnInput.Text;
                if (a.Name == GuestName || a.PpNumber == PassportNum)
                {
                    roomDetailsList.Clear();
                    if (roomDetailsList.Count == 0)
                    {
                        foreach (HotelRoom r in a.RoomList)
                        {
                            roomDetailsList.Add(r.RoomNumber.ToString() + "   " + r.RoomType.ToString() + "   $" + r.DailyRate.ToString());
                        }
                    }

                    arInput.ItemsSource = null;
                    arInput.ItemsSource = bookedrooms;
                }
            }
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        //2.3.1	When the Check-Out button is clicked.The hotel room(s) will be made available.
        //2.3.2	Display a message “Check-Out successful”.

        private void CheckOutBtn_Click(object sender, RoutedEventArgs e)
        {
            roomDetailsList.Clear();
            error.Text = "Check-Out successfull";
        }

        //3.1.2	Display member points and status. Adding on to the Search feature, the guest membership status and points is displayed in a text block.



        //3.1.3	Earning points. Points are earned from the final amount paid by guest for their stay at the hotel. The conversion rate for points to be earned is amount paid during check-out divided by 10. If it is the guest’s first stay, the points cannot be used immediately to offset the first bill.  For example, if Amelia stays a total of 7 nights she pays 7 x $90 = $630 and earns 63 points.

    }
}
