﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    abstract class HotelRoom:Stay
    {
        private string roomType;
        private string roomNumber;
        private string bedConfiguration;
        private double dailyRate;
        private bool isAvail;
        private int noOfOccupants;

        public int NoOfOccupants
        {
            get { return noOfOccupants; }
            set { noOfOccupants = value; }
        }

        public bool IsAvail
        {
            get { return isAvail; }
            set { isAvail = value; }
        }


        public double DailyRate
        {
            get { return dailyRate; }
            set { dailyRate = value; }
        }


        public string BedConfiguration
        {
            get { return bedConfiguration; }
            set { bedConfiguration = value; }
        }


        public string RoomNumber
        {
            get { return roomNumber; }
            set { roomNumber = value; }
        }


        public string RoomType
        {
            get { return roomType; }
            set { roomType = value; }
        }

        public HotelRoom() { }

        public HotelRoom(string roomType, string roomNumber, string bedConfiguration, double dailyRate, bool isAvail, int noOfOccupants)
        {
            RoomType = roomType;
            RoomNumber = roomNumber;
            BedConfiguration = bedConfiguration;
            DailyRate = dailyRate;
            IsAvail = isAvail;
            NoOfOccupants = noOfOccupants;
        }

        public abstract double CalculateCharges();

        public override string ToString()
        {
            return base.ToString();
        }

    }
}
