﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class StandardRoom:HotelRoom
    {
        private bool requireWifi;
        private bool requireBreakfast;

        public bool RequireBreakfast
        {
            get { return requireBreakfast; }
            set { requireBreakfast = value; }
        }

        public bool RequireWifi
        {
            get { return requireWifi; }
            set { requireWifi = value; }
        }

        public StandardRoom() { }

        public StandardRoom(string roomNumber, string bedConfiguration, double dailyRate):base("Standard",roomNumber, bedConfiguration, dailyRate, true, 0)
        {

        }

        public override double CalculateCharges()
        {
            return DailyRate * CalculateTotal();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
