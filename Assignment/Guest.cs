﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class Guest:Stay
    {
        private string name;
        private string ppNumber;
        private Stay hotelStay;
        private Membership membership;
        private bool isCheckedIn;

        public bool IsCheckedIn
        {
            get { return isCheckedIn; }
            set { isCheckedIn = value; }
        }

        public Membership Membership
        {
            get { return membership; }
            set { membership = value; }
        }

        public Stay HotelStay
        {
            get { return hotelStay; }
            set { hotelStay = value; }
        }

        public string PpNumber
        {
            get { return ppNumber; }
            set { ppNumber = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Guest() { }

        public Guest(string name, string ppNumber, Stay hotelStay, Membership membership, bool isCheckedIn)
        {
            Name = name;
            PpNumber = ppNumber;
            Membership = membership;
            IsCheckedIn = isCheckedIn;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
