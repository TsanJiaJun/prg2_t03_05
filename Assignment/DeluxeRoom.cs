﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class DeluxeRoom:HotelRoom
    {
        private bool additionalBed;

        public bool AdditionalBed
        {
            get { return additionalBed; }
            set { additionalBed = value; }
        }

        public DeluxeRoom() { }

        public DeluxeRoom(string roomNumber, string bedConfiguration, double dailyRate) : base("Deluxe", roomNumber, bedConfiguration, dailyRate, true, 0)
        {

        }

        public override double CalculateCharges()
        {
            return DailyRate * CalculateTotal();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
