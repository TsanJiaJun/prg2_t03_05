﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class Membership
    {
        private string status;
        private int points;

        public int Points
        {
            get { return points; }
            set { points = value; }
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public Membership() { }

        public Membership(string status, int points)
        {
            Status = status;
            Points = points;
        }

        public int EarnPoints(double epoints)
        {
            
            Points += Convert.ToInt32(epoints);
            return Points;
        }

        public bool RedeemPoints(int rpoints)
        {
            if (status == "Silver" | status == "Gold")
            {
                return true;
            }
            else
            {
                return false;
            };
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
